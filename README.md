# django-boilerplate

This is the boilerplate we use to start all of our new Django based projects.

## Project Development Setup

Here's how to setup your development environment, replace all instances of `myapp` below with the name of your application. Run all commands from the project root, unless otherwise specified.

### Local Development Initialization

1. Follow the instructions to setup pipenv on your local system: <https://pipenv.readthedocs.io/en/latest/>
1. `$ pipenv install`
1. `$ pipenv shell`

### Vagrant Development Initialization

1. `$ vagrant up`
1. `$ vagrant ssh`
1. `$ cd /vagrant`
1. `$ pipenv install`
1. `$ pipenv shell`

## Create New Project

1. `$ docker-compose run web django-admin.py startproject myproject .`
   1. Replace `myproject` above with your new project name
1. `$ sudo chown -R $USER:$USER .`

## Extending Functionality

### Create New App

1. `$ python manage.py startapp myapp`
   1. Replace `myapp` above with your new app name

### Add New App To Project

`myproject/settings.py`

```json
INSTALLED_APPS = [
    'myapp.apps.MyappConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]
```

### Create Models

https://docs.djangoproject.com/en/2.1/topics/db/models/

`myapp/models.py`

```python
from django.db import models


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)
```

## Development Continuation

1. Run `$ python manage.py migrate`
1. Run `$ python manage.py createsuperuser`

### Models and Schema

If you make changes to the models/schema:

1. Run `$ python manage.py makemigrations myapp`
1. Run `$ python manage.py sqlmigrate myapp 0001`
   - `0001` above is an example
1. Run `$ python manage.py migrate`

### Admin

TBD

## Run Project

### Run Migrations

1. `$ python manage.py makemigrations myapp`
1. `$ python manage.py sqlmigrate myapp 0001`
1. `$ python manage.py migrate`

### Start Dev Server

1. `$ source dev-init.sh`
1. `$ python manage.py migrate`
1. `$ ./run-dev.sh`

## Testing

### Basic Testing

1. `$ python manage.py test myapp`

### Test Client

1. `$ python manage.py shell`

```python
>>> from django.test.utils import setup_test_environment
>>> setup_test_environment()
```

```python
>>> from django.test import Client
>>> # create an instance of the client for our use
>>> client = Client()
```

```python
>>> # get a response from '/'
>>> response = client.get('/')
Not Found: /
>>> # we should expect a 404 from that address; if you instead see an
>>> # "Invalid HTTP_HOST header" error and a 400 response, you probably
>>> # omitted the setup_test_environment() call described earlier.
>>> response.status_code
404
>>> # on the other hand we should expect to find something at '/polls/'
>>> # we'll use 'reverse()' rather than a hardcoded URL
>>> from django.urls import reverse
>>> response = client.get(reverse('polls:index'))
>>> response.status_code
200
>>> response.content
b'\n    <ul>\n    \n        <li><a href="/polls/1/">What&#39;s up?</a></li>\n    \n    </ul>\n\n'
>>> response.context['latest_question_list']
<QuerySet [<Question: What's up?>]>
```
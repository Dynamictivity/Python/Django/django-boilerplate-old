#!/bin/bash

# update packages
apt-get update
apt-get -y upgrade

# install python
apt-get install -y python3-pip build-essential libssl-dev libffi-dev python-dev python3-venv

# update pip
pip3 install --upgrade pip

# install docker
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-cache policy docker-ce
apt-get install -y docker-ce docker-compose
usermod -aG docker ubuntu
